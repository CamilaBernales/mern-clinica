const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const medicoSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: true
    },
    email: {
        type: String,
        trim: true,
        required: true
    },
    especialidad: {
        type: String,
        trim: true,
        required: true
    },
    horarios: [{
        type: String
    }],

    password: {
        type: String,
        trim: true,
        required: true
    },
    aprobado:{
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('Medicos', medicoSchema);