const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const pacienteSchema= new Schema({
    nombre:{
        type: String,
        trim:true,
        required: true

    },
    email:{
        type: String,
        trim:true,
        required: true
    },
    password:{
        type: String,
        trim: true,
        required: true
    },
    aprobado:{
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('Pacientes', pacienteSchema);