const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const turnoSchema = new Schema({
    sintomas: {
        type: String,
        requiered: true,
        trim: true
    },
    medico: {
        type: String,
        trim: true,
        requiered: true
    },
    horario: {
        type: String,
        trim: true,
        requiered: true
    }
});

module.exports = mongoose.model('Turno', turnoSchema);