const jwt = require('jsonwebtoken');


module.exports = (req, res, next) => {
    
    const token = req.header('x-auth-token');

    if(!token){
        res.status(401).json({mensaje: 'No hay Token, acceso denegado'});
    }
    try {
        const payload = jwt.verify(token, process.env.SECRET);
        req.user = payload.user;
        next();

    } catch (error) {
        return res.status(401).json({mensaje: 'Token no valido, acceso denegado.' }); 
    }
}; 

