const mongoose = require('mongoose');
require('dotenv').config(); 

const URI = process.env.DB_MONGO;
const OPTIONS = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
};


mongoose.connect(URI, OPTIONS)
.then(db=> console.log('Base de datos conectada'))
.catch(error => {
    console.log("Base de datos no conectada")
    console.error(error);
    process.exit(1);
});

module.exports = mongoose;