const Turno = require('../modelo/Turno');
const { validationResult } = require('express-validator');

exports.crearTurno = async (req, res, next) => {
    const error = validationResult(req);
    if(!error.isEmpty()){
        return res.status(422).json({error: error.array()});
    }
    try {
        let turno = new Turno(req.body);
        turno.paciente = req.user.id
        await turno.save();
        res.json({mensaje: 'Turno Creado Correctamente'})
    } catch (error) {
        console.error(error);
         res.status(400).json({mensaje: 'Hubo un error'});
     
    }
};