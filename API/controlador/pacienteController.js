const Paciente = require('../modelo/Paciente');
const bcryptjs = require('bcryptjs');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');


exports.crearPaciente = async (req, res, next) => {

    const errores = validationResult(req);
    if (!errores.isEmpty()) {
        return res.status(400).json({ errores: errores.array() })

    }

    const { email, password } = req.body;

    try {
        let paciente = await Paciente.findOne({ email });

        if (paciente) {
            return res.status(400).json({ mensaje: 'Este usuario ya existe' });
        }
        paciente = new Paciente(req.body);
        const salt = await bcryptjs.genSalt(10);
        paciente.password = await bcryptjs.hash(password, salt);
        await paciente.save();

        const payload = {
            user: {
                id: user.id
            }
        };

        // Firmar el token
        jwt.sign(payload, process.env.SECRET, {
            expiresIn: '1h'
        }, (error, token) => {
            if (error) throw error;
            res.json({ msg: 'Usuario creado correctamente', token });
        })


    } catch (error) {
        console.log(error);
        res.status(400).json({ msg: 'Hubo un error'});


    }
}

exports.obtenerPacientes = async (req, res, next) => {
    try {
        const pacientes = await Paciente.find({});
        res.json(pacientes);
    } catch (error) {
        console.log(error);

    }
}

exports.actualizarPaciente = async (req, res, next) => {
    try {
        const paciente = await Paciente.findByIdAndUpdate({ _id: req.params.id }, req.body, {
            new: true
        })
        res.json(paciente);
    } catch (error) {
        console.log(error);

    }
}

exports.loginPaciente = async (req, res, next) => {

    const errores = validationResult(req);
    
    if(!errores.isEmpty()){
        return res.status(400).json({errores: errores.array()})
    }

    const {email, password} = req.body;

    try {

        let user = await Paciente.findOne({email});
        if(!user){
            console.log('Email no valido');
            res.status(400).json({mensaje: 'El email o contreña no son validos'})
        }
        if(user.aprobado === false){
            res.status(400).json({mensaje: 'Usuario no aprobado por el administrador'})
        }

        const passCorrecto = await bcryptjs.compare(password, user.password)
        if(!passCorrecto){
            console.log('password no valido');
            res.status(400).json({mensaje: 'El email o contraseña no son validos. '})
        }

        const payload = {
            user: {
                id: user.id
            }
        };

        jwt.sign(payload, process.env.SECRET, {
            expiresIn: '1h'
        }, (error, token) => {
            if(error) throw error;

            //mensaje de confirmacion
            res.json({token})
        })
    } catch (error) {
        console.log(error);
        res.status(400).json({ msg: 'Hubo un error'});
    }
};

exports.eliminarPaciente = async(req, res) => {

    try {
        await Paciente.findOneAndDelete({_id:req.params.id});
        res.json({mensaje: 'El paciente fue eliminado.'})
    } catch (error) {
        console.log(error)
    }
}


