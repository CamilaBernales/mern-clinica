const Medico = require('../modelo/Medico');
const bcryptjs = require('bcryptjs');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

exports.crearMedico = async (req, res, next) => {

    const errores = validationResult(req);
    if (!errores.isEmpty()) {
        return res.status(400).json({ errores: errores.array() })

    }

    const { email, password } = req.body;

    try {
        let medico = await Medico.findOne({ email });

        if (medico) {
            return res.status(400).json({ mensaje: 'Este usuario ya existe' });
        }
        medico = new Medico(req.body);
        const salt = await bcryptjs.genSalt(10);
        medico.password = await bcryptjs.hash(password, salt);
        await medico.save();

        res.json({ mensaje: 'Se creo un medico' });

    } catch (error) {
        console.log(error);
        next();

    }
}

exports.obtenerMedicos = async (req, res, next) => {
    try {
        const medicos = await Medico.find({});
        const especialidades = []
        medicos.forEach(medico => {
            if (especialidades.indexOf(medico.especialidad) < 0) {
                especialidades.push(medico.especialidad);
            }
        });

        res.json({ especialidades, medicos });
    } catch (error) {
        console.log(error);
        next();
    }
}

exports.actualizarMedico = async (req, res, next) => {
    try {
        const medico = await Medico.findByIdAndUpdate({ _id: req.params.id }, req.body, {
            new: true
        })
        res.json(medico);
    } catch (error) {
        console.log(error);
        next();
    }
}

exports.obtenerMedico = async (req, res, next) => {

    const email = req.body.email
    const password = req.body.password;

    Medico.findOne({ email }).then(user => {
        if (!user) {
            return res.status(400).json({ mensaje: "Email no encontrado o Usuario" });
        }
        if (user.aprobado === false) {
            return res.status(400).json({ mensaje: 'Usuario no aprobado por el administrador' })
        }
        bcryptjs.compare(password, user.password).then(match => {
            if (match) {
                // Crear y firmar el JWT
                const payload = {
                    user: {
                        id: user.id
                    }
                };
                // Firmar token
                jwt.sign(payload, process.env.SECRET, {
                    expiresIn: '1h'
                }, (error, token) => {
                    if (error) throw error;
                    // Mensaje de confirmación
                    res.json({ token });
                });
            } else {
                return res.status(400).json({ mensaje: 'Contraseña Incorrecta' })
            }
        });
    });
}

exports.filtrarMedico = async (req, res) => {

    const { especialidad } = req.params;

    try {
        const filtro = await Medico.find({ especialidad: especialidad })
        res.json(filtro);
    } catch (error) {
        console.log(error);
        next();
    }
}

exports.eliminarMedico = async(req, res) => {
    try {
        await Medico.findOneAndDelete({_id : req.params.id});
        res.json({mensaje: 'El médico fue eliminado'})
    } catch (error) {
        console.log(error);
    }
}

