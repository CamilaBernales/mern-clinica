const express = require('express');
const router = express.Router();
const medicoController = require('../controlador/medicoController');
const { check } = require('express-validator');

router.post('/medicos',[
    check('nombre', 'El nombres es obligatorio').not().isEmpty(),
    check('email','El email es obligatorio.').not().isEmpty(),
    check('email','Ingrese un email válido.').isEmail(),
    check('password','El email es obligatorio.').not().isEmpty(),
    check('password','El password debe ser mínimo de 6 caracteres.').isLength({ min: 6})
],
medicoController.crearMedico
);
router.get('/medicos',
medicoController.obtenerMedicos
);
router.put('/medicos/:id',
medicoController.actualizarMedico
);
router.post('/medicos/login',
medicoController.obtenerMedico
);
router.get('/medicos/filtro/:especialidad',
medicoController.filtrarMedico
);
router.delete('/medicos/:id',
medicoController.eliminarMedico
);

module.exports = router;
