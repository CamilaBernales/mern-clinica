const express = require('express');
const router = express.Router();
const pacienteController = require('../controlador/pacienteController');
const { check } = require('express-validator');

//crear un paciente api/paciente

router.post('/pacientes', [

    check('nombre', 'El nombres es obligatorio').not().isEmpty(),
    check('email','El email es obligatorio.').not().isEmpty(),
    check('email','Ingrese un email válido.').isEmail(),
    check('password','El email es obligatorio.').not().isEmpty(),
    check('password','El password debe ser mínimo de 6 caracteres.').isLength({ min: 6})
],

pacienteController.crearPaciente
);

router.get('/pacientes',
pacienteController.obtenerPacientes
);
router.put('/pacientes/:id',
pacienteController.actualizarPaciente
);
router.post('/pacientes/login',
pacienteController.loginPaciente
);
router.delete('/pacientes/:id',
pacienteController.eliminarPaciente
);
module.exports = router;