const express = require('express');
const router = express.Router();
const turnoController = require('../controlador/turnoController');
const {check} = require('express-validator');
const auth = require('../middlelwares/auth');


router.post('/turnos',
 auth, [
    check('sintomas', 'Los sintomas son obligatorios').not().isEmpty(),
    check('medico', 'El medico es obligatorio').not().isEmpty(),
    check('horario', 'El horario es obligatorio').not().isEmpty()
],
turnoController.crearTurno
);

module.exports = router;