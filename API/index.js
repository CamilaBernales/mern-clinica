const express = require('express');
const morgan = require('morgan')
const cors = require('cors');
const pacientesRoute = require('./routes/pacientes')
const medicosRoute = require('./routes/medicos')
const turnosRoute = require('./routes/turnos')
const mongoose = require('./database');
const bodyParser = require('body-parser');

const server = express();
server.use(cors());
//puerto 
const PORT = process.env.PORT;

server.use(morgan('dev'))
server.use(express.json()); // for parsing serverlication/json
server.use(express.urlencoded({ extended: true})); // for parsing application/x-www-form-urlencoded

//rutas
server.use('/', pacientesRoute);
server.use('/', medicosRoute);
server.use('/', turnosRoute);



server.listen(PORT, () => {
    console.log('servidor funcionado');
})

